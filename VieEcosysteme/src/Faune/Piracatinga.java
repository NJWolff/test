package Faune;

import java.util.ArrayList;

import java.util.List;

import terrain.Case;
import terrain.Grille;
import terrain.Terrain;

public class Piracatinga extends Charognard {

	/**
	 * Constructeur de la classe Piracatinga
	 * sexe aléatoire
	 * @param id
	 */
	public Piracatinga(int id,Case p) {
		this.setAge(0);
		this.setId(id);
		if (Math.random()<0.5) {
			this.setSexe("female");	
		}
		else {
			this.setSexe("male");
			
		}
		this.setEspece("piracatinga");
		this.setTpsRepos(0);
		this.setTpsMinimumRepro(52);
		this.setAgeMax(1040);
		this.setPm(7);
		this.isAlive(true);
		this.setSante(15);
		this.setSeuilCritique(5);
		List<Terrain> list = new ArrayList<Terrain>();
		list.add(Terrain.EAU);
		this.setTerrainPossible(list);
		
		this.setPosition(p);	
		this.setNbMaxPetit(10);
	}
	
	
	@Override
	public void seReproduire(Faune f) {
		// TODO Auto-generated method stub

		List<Animal> listpetits = new ArrayList<Animal>();
		for (int k=0 ;  k<this.getNbMaxPetit(); k++) {
			int id = 0;
			listpetits.add(new Piracatinga(id, this.getPosition()));
		}
		f.getListeAnimaux().addAll(listpetits);
		
	}

	@Override
	public void seDeplacer(Case c,Grille g ) {
		// TODO Auto-generated method stub
		
	}

	

}

