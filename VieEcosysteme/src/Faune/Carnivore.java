package Faune;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import terrain.Case;
import terrain.Grille;

public abstract class Carnivore extends Animal {

	
	
	@Override
	public Case reperageNourriture(Grille g) {
		int portee = this.getPm()+3;
		List<Case> ListeCases = new ArrayList<Case>();
		int repartition = (int) portee/2;
		int ligneDepart = this.getPosition().getLigne()-repartition;
		int colonneDepart =this.getPosition().getColonne()-repartition;
		int ligneArrivee = this.getPosition().getColonne()-repartition+portee-1;
		int colonneArrivee = this.getPosition().getColonne()-repartition+portee-1;
		for (int i=0; ligneDepart+i<ligneArrivee; i++) {
			for(int j=0; colonneDepart+j<colonneArrivee;j++) {
				ListeCases.add(g.getQuadrillage()[i][j]);
			}
		}
		List<Case> casesNourriture = new ArrayList<Case>();
		List<Double> distNourriture = new ArrayList<Double>();
		
		for (Case c : ListeCases) {
			List<Animal> contenu = c.getContenu();
			for (Animal a : contenu) {
				
				if (a.isAlive()==true  && this.getTerrainPossible().contains(c.getTerrainRéel()) && !a.getEspece().equals(this.getEspece())) {
					casesNourriture.add(c);
					double dist = Math.sqrt(Math.pow((this.getPosition().getLigne()-c.getLigne()),2)+Math.pow((this.getPosition().getColonne()-c.getColonne()),2));
					distNourriture.add(dist);
				
				}
			}
		}
		int index = distNourriture.indexOf(Collections.min(distNourriture));
		return casesNourriture.get(index);
	}
	
	
	
	@Override
	public void seDeplacer(Case c, Grille g) {
		int ligneDepart = this.getPosition().getLigne();
		int colonneDepart =this.getPosition().getColonne();
		int ligneArrivee = c.getColonne();
		int colonneArrivee = c.getColonne();
		for (int k=0; k<this.getPm(); k++) {
			int diffLigne= ligneArrivee -ligneDepart;
			if ( diffLigne>0 && this.getTerrainPossible().contains(g.getQuadrillage()[ligneDepart+1][colonneDepart].getTerrainRéel())) {
				ligneDepart++;
			}
			else if ( diffLigne<0 && this.getTerrainPossible().contains(g.getQuadrillage()[ligneDepart-1][colonneDepart].getTerrainRéel())) {
				ligneDepart--;
			}
			else  {
				int diffColonne = colonneArrivee -colonneDepart ;
				if ( diffColonne>0 && this.getTerrainPossible().contains(g.getQuadrillage()[ligneDepart][colonneDepart+1].getTerrainRéel())) {
					colonneDepart++;
				}
				else if ( diffColonne<0 && this.getTerrainPossible().contains(g.getQuadrillage()[ligneDepart][colonneDepart-1].getTerrainRéel())) {
					colonneDepart--;
				}
		
			}
		}
		Case caseFinale= g.getQuadrillage()[ligneDepart][colonneDepart];
		this.setPosition(caseFinale);	
		
	}
}
