package Faune;

import java.util.ArrayList;

import java.util.List;

import terrain.Case;
import terrain.Grille;
import terrain.Terrain;

public class Martre extends Carnivore{

	/**
	 * Constructeur de la classe Matre
	 * sexe aléatoire
	 * @param id
	 */
	public Martre(int id,Case p) {
		this.setAge(0);
		this.setId(id);
		if (Math.random()<0.5) {
			this.setSexe("female");	
		}
		else {
			this.setSexe("male");
			
		}
		this.setEspece("martre");
		this.setTpsRepos(0);
		this.setTpsMinimumRepro(52);
		this.setAgeMax(572);
		this.setPm(5);
		this.isAlive(true);
		this.setSante(15);
		this.setSeuilCritique(5);
		List<Terrain> list = new ArrayList<Terrain>();
		
		list.add(Terrain.PLAINE);
		list.add(Terrain.FORÊT);
		list.add(Terrain.VOLCAN);
		this.setTerrainPossible(list);
		
		this.setPosition(p);	
		this.setNbMaxPetit(6);
	}
	
	@Override
	public void seReproduire(Faune f) {
		// TODO Auto-generated method stub

		List<Animal> listpetits = new ArrayList<Animal>();
		for (int k=0 ;  k<this.getNbMaxPetit(); k++) {
			int id = 0;
			listpetits.add(new Martre(id, this.getPosition()));
		}
		f.getListeAnimaux().addAll(listpetits);
	}

	@Override
	public void seDeplacer(Case c,Grille g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void seNourrir(Faune f) {
		// TODO Auto-generated method stub
		Case c =this.getPosition();
				
		for (Animal a : c.getContenu()) {
			if(a.isAlive()==true && !this.getEspece().equals(a.getEspece())) {
				this.setSante(this.getSante()+1);
				a.isAlive(false);
				a.setAge(0); //Réinitialisation de l'age de l'animal mangé
				//Comment s'assurer que la faune soit modifiée ?
			}
			break;
		}

	}
}



