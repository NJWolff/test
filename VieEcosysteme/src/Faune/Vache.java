package Faune;

import java.util.ArrayList;

import java.util.List;

import terrain.Case;
import terrain.Grille;
import terrain.Terrain;

public class Vache extends Herbivore {
	
	
	/**
	 * Constructeur de la classe Vache
	 * sexe aléatoire
	 * @param id
	 */
	public Vache(int id,Case p) {
		this.setAge(0);
		this.setId(id);
		if (Math.random()<0.5) {
			this.setSexe("female");	
		}
		else {
			this.setSexe("male");
			
		}
		this.setEspece("vache");
		this.setTpsRepos(0);
		this.setTpsMinimumRepro(52);
		this.setAgeMax(624);
		this.setPm(10);
		this.isAlive(true);
		this.setSante(10);
		this.setSeuilCritique(3);
		List<Terrain> list = new ArrayList<Terrain>();
		list.add(Terrain.PLAINE);
		list.add(Terrain.FORÊT);
		this.setTerrainPossible(list);
		this.setNbMaxPetit(1);
		this.setPosition(p);	
	
	}

	@Override
	public void seReproduire( Faune f) {
		// TODO Auto-generated method stub

		List<Animal> listpetits = new ArrayList<Animal>();
		for (int k=0 ;  k<this.getNbMaxPetit(); k++) {
			int id = 0;
			listpetits.add(new Vache(id, this.getPosition()));
		f.getListeAnimaux().addAll(listpetits);
		
		}
		
	}

	@Override
	public void seDeplacer(Case c,Grille g) {
		// TODO Auto-generated method stub
		
	}

	
	
	
	
	

}
