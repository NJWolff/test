package Faune;

import java.util.ArrayList;
import java.util.List;
/**
 * Classe servant à décrire l'ensemble de la Faune dans l'écosystème
 * @author Constance Barban, Nathan Wolff, Quentin Le Port
 *
 */
public class Faune {
	/**
	 * Liste de l'ensemble des animaux de l'écosystème  (tant qu'ils sont sur la carte : contient les vivants et les morts)
	 */
	private List<Animal> listeAnimaux; 
	
	
	/**
	 * Constructeur de la classe Faune
	 * @param listeAnimaux
	 */
	public Faune() {
		this.listeAnimaux = new ArrayList<Animal>();
	}
	
	
	/**
	 * Constructeur de la classe Faune
	 * @param listeAnimaux
	 */
	public Faune(List<Animal> listeAnimaux) {
		this.listeAnimaux = listeAnimaux;
	}
	
	



	public List<Animal> getListeAnimaux() {
		return listeAnimaux;
	}


	public void setListeAnimaux(List<Animal> listeAnimaux) {
		this.listeAnimaux = listeAnimaux;
	}
	//ajout de getter et setter dans Faune pour essayer de récupérer la liste dans la classe Charognard


	/**
	 * Méthode permettant d'ajouter un nouvel animal dans la faune
	 * @param animal de type Animal correspond à un n'importe quel animal qui hérite de la classe Animal
	 */
	public void add(Animal animal) {
		this.listeAnimaux.add(animal);
	}
	
	/**
	 * Méthode qui ne sert visiblement à rien 
	 * Méthode permettant d'ajouter plusieurs animaux dans la faune
	 * @param animal de type List<Animal> correspond à une list de  n'importe quels animaux qui héritent de la classe Animal
	 */
	public void add(List<Animal> listanimal) {
		for ( Animal a : listanimal) {
			this.listeAnimaux.add(a);
		}
		
	}
	
	
	/**
	 * Méthode permettant de supprimer de la liste les animaux morts dont le temps de décomposition est supérieur à 5tours (on supprime ceux qui seront aussi supprimés de la carte et qui ne peuvent plus être une source de nourriture pour les Charognards)
	 */
	public void delete() { // mettre un static?
		for (Animal animal : this.listeAnimaux) {
			if (animal.isAlive() != true) {
				if (animal.getAge()>=5) {
					this.listeAnimaux.remove(animal);
				}
			}
		}
	}
	
	/**
	 * Méthode permettant d'initialiser la base de données
	 */
	public void initialisationBDD() {
		}
	/**
	 * Méthode permettant de mettre à jour la base de données sans droper les tables systématiquement
	 */
	public void miseAjour() {
		}
	}
