package Faune;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import terrain.Case;
import terrain.Grille;

public abstract class Herbivore extends Animal {
	
	/**
	 * Méthode permettant aux herbivores de repérer les prédateurs potentiels à proximité pour se déplacer dans la direction opposée
	 * @return Case renvoie la case du prédateur
	 */
	public Case reperagePredateur(Grille g) {
			int portee = this.getPm()+3;
			List<Case> ListeCases = new ArrayList<Case>();
			int repartition = (int) portee/2;
			int ligneDepart = this.getPosition().getLigne()-repartition;
			int colonneDepart =this.getPosition().getColonne()-repartition;
			int ligneArrivee = this.getPosition().getColonne()-repartition+portee-1;
			int colonneArrivee = this.getPosition().getColonne()-repartition+portee-1;
			for (int i=0; ligneDepart+i<ligneArrivee; i++) {
				for(int j=0; colonneDepart+j<colonneArrivee;j++) {
					ListeCases.add(g.getQuadrillage()[i][j]);
				}
			}
			List<Case> casesPredateur = new ArrayList<Case>();
			List<Double> distPredateur = new ArrayList<Double>();
			
			for (Case c : ListeCases) {
				for (Animal a : c.getContenu()) {
					if (a instanceof Carnivore) {
						casesPredateur.add(c);
						double dist = Math.sqrt(Math.pow((this.getPosition().getLigne()-c.getLigne()),2)+Math.pow((this.getPosition().getColonne()-c.getColonne()),2));
						distPredateur.add(dist);
					}
				}
				
			}
			int index = distPredateur.indexOf(Collections.min(distPredateur));
			return casesPredateur.get(index);
	
		
	}
	/**
	 * Méthode se nourrir des herbivores qui augmente leur santé et diminue le nombre de nourriture de la case sur laquelle est l'herbivore     
	 */
	@Override
	public void seNourrir( Faune f) {
		// TODO Auto-generated method stub
		this.setSante(this.getSante()+1);
		int nourriture = this.getPosition().getNourriture();
		this.getPosition().setNourriture(nourriture-1);
	
	}
	
	/**
	 * Methode de repérage de la Nourriture 
	 */
	@Override
	public Case reperageNourriture(Grille g) {
		int portee = this.getPm()+3;
		List<Case> ListeCases = new ArrayList<Case>();
		int repartition = (int) portee/2;
		int ligneDepart = this.getPosition().getLigne()-repartition;
		int colonneDepart =this.getPosition().getColonne()-repartition;
		int ligneArrivee = this.getPosition().getColonne()-repartition+portee-1;
		int colonneArrivee = this.getPosition().getColonne()-repartition+portee-1;
		for (int i=0; ligneDepart+i<ligneArrivee; i++) {
			for(int j=0; colonneDepart+j<colonneArrivee;j++) {
				ListeCases.add(g.getQuadrillage()[i][j]);
			}
		}
		List<Case> casesNourriture = new ArrayList<Case>();
		List<Double> distNourriture = new ArrayList<Double>();
		
		for (Case c : ListeCases) {
			if (c.getNourriture() !=0 && this.getTerrainPossible().contains(c.getTerrainRéel())) {
				casesNourriture.add(c);
				double dist = Math.sqrt(Math.pow((this.getPosition().getLigne()-c.getLigne()),2)+Math.pow((this.getPosition().getColonne()-c.getColonne()),2));
				distNourriture.add(dist);
			}
		}
		int index = distNourriture.indexOf(Collections.min(distNourriture));
		
		return casesNourriture.get(index);
	}
	
	@Override
	public void seDeplacer(Case c,Grille g) {
		int ligneDepart = this.getPosition().getLigne();
		int colonneDepart =this.getPosition().getColonne();
		int ligneArrivee = c.getColonne();
		int colonneArrivee = c.getColonne();
		for (int k=0; k<this.getPm(); k++) {
			int diffLigne= ligneArrivee -ligneDepart;
			if ( diffLigne>0 && this.getTerrainPossible().contains(g.getQuadrillage()[ligneDepart+1][colonneDepart].getTerrainRéel())) {
				ligneDepart++;
			}
			else if ( diffLigne<0 && this.getTerrainPossible().contains(g.getQuadrillage()[ligneDepart-1][colonneDepart].getTerrainRéel())) {
				ligneDepart--;
			}
			else  {
				int diffColonne = colonneArrivee -colonneDepart ;
				if ( diffColonne>0 && this.getTerrainPossible().contains(g.getQuadrillage()[ligneDepart][colonneDepart+1].getTerrainRéel())) {
					colonneDepart++;
				}
				else if ( diffColonne<0 && this.getTerrainPossible().contains(g.getQuadrillage()[ligneDepart][colonneDepart-1].getTerrainRéel())) {
					colonneDepart--;
				}
		
			}
		}
		Case caseFinale= g.getQuadrillage()[ligneDepart][colonneDepart];
		this.setPosition(caseFinale);	
		
		
	}

	
}
