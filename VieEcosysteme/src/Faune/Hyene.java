package Faune;

import java.util.ArrayList;

import java.util.List;

import terrain.Case;
import terrain.Grille;
import terrain.Terrain;

public class Hyene extends Charognard{

	
	/**
	 * Constructeur de la classe Hyène
	 * sexe aléatoire
	 * @param id
	 */
	public Hyene(int id,Case p) {
		this.setAge(0);
		this.setId(id);
		if (Math.random()<0.5) {
			this.setSexe("female");	
		}
		else {
			this.setSexe("male");
			
		}
		this.setEspece("hyene");
		this.setTpsRepos(0);
		this.setTpsMinimumRepro(82);
		this.setAgeMax(1040);
		this.setPm(7);
		this.isAlive(true);
		this.setSante(15);
		this.setSeuilCritique(5);
		List<Terrain> list = new ArrayList<Terrain>();
		
		list.add(Terrain.PLAINE);
		list.add(Terrain.FORÊT);
		list.add(Terrain.VOLCAN);
		list.add(Terrain.PONT);
		this.setTerrainPossible(list);
		
		this.setPosition(p);	
		this.setNbMaxPetit(4);
	}
	
	
	@Override
	public void seReproduire(Faune f) {
		// TODO Auto-generated method stub

		List<Animal> listpetits = new ArrayList<Animal>();
		for (int k=0 ;  k<this.getNbMaxPetit(); k++) {
			int id = 0;
			listpetits.add(new Hyene(id, this.getPosition()));
		}
		f.getListeAnimaux().addAll(listpetits);
		
	}

	@Override
	public void seDeplacer(Case c,Grille g ) {
		// TODO Auto-generated method stub
		
	}

	

}
