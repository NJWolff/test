package Faune;

import java.util.ArrayList;

import java.util.List;


import terrain.Case;
import terrain.Grille;
import terrain.Terrain;

public class Truite extends Herbivore{

	/**
	 * Constructeur de la classe Truite
	 * sexe aléatoire
	 * @param id
	 */
	public Truite(int id,Case p) {
		this.setAge(0);
		this.setId(id);
		if (Math.random()<0.5) {
			this.setSexe("female");	
		}
		else {
			this.setSexe("male");
			
		}
		this.setEspece("truite");
		this.setTpsRepos(0);
		this.setTpsMinimumRepro(52);
		this.setAgeMax(1040);
		this.setPm(10);
		this.isAlive(true);
		this.setSante(10);
		this.setSeuilCritique(3);
		List<Terrain> list = new ArrayList<Terrain>();
		list.add(Terrain.EAU);
		list.add(Terrain.PONT);
		this.setTerrainPossible(list);
		
		this.setPosition(p);	
		this.setNbMaxPetit(20);
	}
	
	/**
	 * Fonction permettant la reproduction, c'est à dire la création de nouveaux
	 *  individus
	 * 
	 */
	@Override
	public void seReproduire( Faune f) {
		
		List<Animal> listpetits = new ArrayList<Animal>();
		for (int k=0 ;  k<this.getNbMaxPetit(); k++) {
			int id = 0;
			listpetits.add(new Truite(id, this.getPosition()));
		}
		f.getListeAnimaux().addAll(listpetits);	
	}

	@Override
	public void seDeplacer(Case c,Grille g) {
		// TODO Auto-generated method stub
		
	}


		
	


	

}
