package Faune;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import terrain.Case;
import terrain.Grille;

public abstract class Charognard extends Animal {
	

	public Case reperagePredateur(Grille g) {
			int portee = this.getPm()+3;
			List<Case> ListeCases = new ArrayList<Case>();
			int repartition = (int) portee/2;
			int ligneDepart = this.getPosition().getLigne()-repartition;
			int colonneDepart =this.getPosition().getColonne()-repartition;
			int ligneArrivee = this.getPosition().getColonne()-repartition+portee-1;
			int colonneArrivee = this.getPosition().getColonne()-repartition+portee-1;
			for (int i=0; ligneDepart+i<ligneArrivee; i++) {
				for(int j=0; colonneDepart+j<colonneArrivee;j++) {
					ListeCases.add(g.getQuadrillage()[i][j]);
				}
			}
			List<Case> casesPredateur = new ArrayList<Case>();
			List<Double> distPredateur = new ArrayList<Double>();
			
			for (Case c : ListeCases) {
				for (Animal a : c.getContenu()) {
					if (a instanceof Carnivore) {
						casesPredateur.add(c);
						double dist = Math.sqrt(Math.pow((this.getPosition().getLigne()-c.getLigne()),2)+Math.pow((this.getPosition().getColonne()-c.getColonne()),2));
						distPredateur.add(dist);
					}
				}
				
			}
			int index = distPredateur.indexOf(Collections.min(distPredateur));
			return casesPredateur.get(index);
	
		
	}
 
	/**
	 * Redéfinition de la méthode seNourrir de la classe Animal: elle prend en paramètre la faune afin de pouvoir cleaner la case 
	 * et la liste des animaux (on part du principe que lorsque les charognards ont mangé ils ont supprimé la carcasse
	 */
	@Override
	public void seNourrir(Faune f) {
		for (Animal a : this.getPosition().getContenu()) {
			if(a.isAlive()!=true && !this.getEspece().equals(a.getEspece())) {
				this.setSante(this.getSante()+1);
				this.getPosition().getContenu().remove(a);
				f.getListeAnimaux().remove(a);
			}
			break;
			
		}
	}
	

	@Override
	public Case reperageNourriture(Grille g) {
		int portee = this.getPm()+3;
		List<Case> ListeCases = new ArrayList<Case>();
		int repartition = (int) portee/2;
		int ligneDepart = this.getPosition().getLigne()-repartition;
		int colonneDepart =this.getPosition().getColonne()-repartition;
		int ligneArrivee = this.getPosition().getColonne()-repartition+portee-1;
		int colonneArrivee = this.getPosition().getColonne()-repartition+portee-1;
		for (int i=0; ligneDepart+i<ligneArrivee; i++) {
			for(int j=0; colonneDepart+j<colonneArrivee;j++) {
				ListeCases.add(g.getQuadrillage()[i][j]);
			}
		}
		List<Case> casesNourriture = new ArrayList<Case>();
		List<Double> distNourriture = new ArrayList<Double>();
		
		for (Case c : ListeCases) {
			List<Animal> contenu = c.getContenu();
			for (Animal a : contenu) {
				
				if (a.isAlive()==false  && this.getTerrainPossible().contains(c.getTerrainRéel()) && !a.getEspece().equals(this.getEspece())) {
					casesNourriture.add(c);
					double dist = Math.sqrt(Math.pow((this.getPosition().getLigne()-c.getLigne()),2)+Math.pow((this.getPosition().getColonne()-c.getColonne()),2));
					distNourriture.add(dist);
				
				}
			}
		}
		int index = distNourriture.indexOf(Collections.min(distNourriture));
		return casesNourriture.get(index);
	}
	
}
