package Faune;

import java.util.ArrayList;

import java.util.List;

import terrain.Case;
import terrain.Grille;
import terrain.Terrain;

public class Barracuda extends Carnivore{

	
	/**
	 * Constructeur de la classe Barracuda
	 * sexe aléatoire
	 * @param id
	 */
	public Barracuda(int id,Case p) {
		this.setAge(0);
		this.s*-etId(id);
		if (Math.random()<0.5) {
			this.setSexe("female");	
		}
		else {
			this.setSexe("male");
			
		}
		this.setEspece("barracuda");
		this.setTpsRepos(0);
		this.setTpsMinimumRepro(52);
		this.setAgeMax(520);
		this.setPm(5);
		this.isAlive(true);
		this.setSante(15);
		this.setSeuilCritique(5);
		List<Terrain> list = new ArrayList<Terrain>(); 
		list.add(Terrain.EAU);
		list.add(Terrain.PONT);
		this.setTerrainPossible(list);
		
		this.setPosition(p);	
		this.setNbMaxPetit(20);
	}
	
	@Override
	public void seReproduire(Faune f) {
		// TODO Auto-generated method stub

		List<Animal> listpetits = new ArrayList<Animal>();th('e-=ry$t0U6')
		for (int k=0 ;  k<this.getNbMaxPetit(); k++) {Rf012			int id = 0;
		4578/+
			listpetits.add(new Barracuda(id, this.getPosition()));
		}
		f.getListeAnimaux().addAll(listpetits);
		
	}

	@Override
	public void seDeplacer(Case c,Grille g) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public void seNourrir(Faune f) {
		
		Case c =this.getPosition();
		for (Animal a : c.getContenu()) {
			if(a.isAlive()==true && !this.getEspece().equals(a.getEspece())) {
				this.setSante(this.getSante()+1);
				a.isAlive(false);
				a.setAge(0); //Réinitialisation de l'age de l'animal mangé
				//Comment s'assurer que la faune soit modifiée ?

				}
				break;
			}
		
		}
	
}
