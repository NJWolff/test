package Faune;

import java.util.ArrayList;

import java.util.List;

import terrain.Case;
import terrain.Grille;
import terrain.Terrain;

public class Ours extends Carnivore{

	/**
	 * Constructeur de la classe Ours
	 * sexe aléatoire
	 * @param id
	 */
	public Ours(int id,Case p) {
		this.setAge(0);
		this.setId(id);
		if (Math.random()<0.5) {
			this.setSexe("female");	
		}
		else {
			this.setSexe("male");
			
		}
		this.setEspece("ours");
		this.setTpsRepos(0);
		this.setTpsMinimumRepro(52);
		this.setAgeMax(1024);
		this.setPm(5);
		this.isAlive(true);
		this.setSante(15);
		this.setSeuilCritique(5);
		List<Terrain> list = new ArrayList<Terrain>();
		list.add(Terrain.EAU);
		list.add(Terrain.PLAINE);
		list.add(Terrain.FORÊT);
		list.add(Terrain.VOLCAN);
		this.setTerrainPossible(list);
		
		this.setPosition(p);	
		this.setNbMaxPetit(2);
	}
	
	@Override
	public void seReproduire(Faune f) {
		// TODO Auto-generated method stub

		List<Animal> listpetits = new ArrayList<Animal>();
		for (int k=0 ;  k<this.getNbMaxPetit(); k++) {
			int id = 0;
			listpetits.add(new Ours(id, this.getPosition()));
		f.getListeAnimaux().addAll(listpetits);
		
		}
	}

	@Override
	public void seDeplacer(Case c,Grille g) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void seNourrir(Faune f) {
		// TODO Auto-generated method stub
		Case c =this.getPosition();
		
		if (this.getPosition().getTerrainRéel() == Terrain.EAU ){
			System.out.println("L'ours pèche");
			for (Animal a : c.getContenu()) {
				if(a.isAlive()==true && !this.getEspece().equals(a.getEspece())) {
					this.setSante(this.getSante()+1);
					a.isAlive(false);
					a.setAge(0); //Réinitialisation de l'age de l'animal mangé
					//Comment s'assurer que la faune soit modifiée ?
					
				}
				break;
			}
			}
			
		else {
							
			for (Animal a : c.getContenu()) {
				if(a.isAlive()==true && !this.getEspece().equals(a.getEspece())) {
					this.setSante(this.getSante()+1);
					a.isAlive(false);
					a.setAge(0); //Réinitialisation de l'age de l'animal mangé
					//Comment s'assurer que la faune soit modifiée ?
				}
				break;
				}
		
		}
	}

}
