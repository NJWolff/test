package Faune;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import terrain.Case;
import terrain.Grille;
import terrain.Terrain;

public abstract class Animal {
	/**
	 * identifiant de chaque animal
	 */
	private int id; 
	/**
	 * âge de chaque animal
	 */
	private int age;
	/**
	 * âge maximal pouvant être atteint, au delà, l'animal meurt 
	 */
	private int ageMax;
	/**
	 * correspond à une chaîne de caractères : "mâle" ou "femmelle"
	 */
	private String sexe;
	/**
	 * Point de mouvement de chaque animal: un point de mouvement correspond au deplacement d'une case
	 */
	private int pm;
	/**
	 * boolean qui permet de savoir si l'animal est mort ou vivant
	 */
	private boolean alive;
	/**
	 * nombre de point de vie de chaque animal (rappel: ils seront plus important pour les Charognards)
	 */
	private int sante;
	/**
	 * seuil de santé au dessous duquel l'animal choisit forcémént de manger
	 * au lieu de se reproduire
	 */
	private int seuilCritique;
	/**
	 * Liste des Terrains sur lesquels chaque animal peut ou non se déplacer
	 */
	private List<Terrain> terrainPossible; 
	/**
	 * attribut permettant de connaître la position de l'animal sur la grille
	 */
	private Case position;
	/**
	 * tpsRepos correspond à un compteur de temps incrémenté depuis la derniere reproduction
	 */
	private int tpsRepos;
	/**
	 * seuil de temps a respecter pour pouvoir se reproduire à nouveau
	 * constante dépendant de l'espèce (voir tableau)
	 */
	private int tpsMinimumRepro;
	/**
	 * attribut correspondant au nom de l'espece , pour accès plus facile 
	 */
	private String espece;
	/**
	 * attribut correspondant au nombre maximal de petits par portée 
	 */
	private int nbMaxPetit;
	
	
	
	
	
	/**
	 * *********Getter*********
	 * @return int id animal
	 */
	public int getId() {
		return id;
	}
	/**
	 * *********Setter*********
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}
	
	/**
	 * *********Getter*********
	 * @return int age animal
	 */
	public int getAge() {
		return age;
	}
	/**
	 * *********Setter*********
	 * @param age
	 */
	public void setAge(int age) {
		this.age = age;
	}
	
	/**
	 * *********Getter*********
	 * @return String sexe animal
	 */
	public String getSexe() {
		return sexe;
	}
	/**
	 * *********Setter*********
	 * @param sexe
	 */
	public void setSexe(String sexe) {
		this.sexe = sexe;
	}
	/**
	 * *********Getter*********
	 * @return int nombre de points de mouvement par animal
	 */	
	public int getPm() {
		return pm;
	}
	/**
	 * *********Setter*********
	 * @param pm
	 */
	public void setPm(int pm) {
		this.pm = pm;
	}
	
	/**
	 * *********Getter*********
	 * @return boolean état de l'animal
	 * 
	 */
	public boolean isAlive() {
		return alive;
	}
	/**
	 * *********Setter*********
	 * @param animalVivant
	 */
	public void isAlive(boolean alive) {
		this.alive = alive;
	}
	/**
	 * *********Getter*********
	 * @return int nombre de point de vie de chaque animal
	 */
	public int getSante() {
		return sante;
	}
	/**
	 * *********Setter*********
	 * @param sante
	 */
	public void setSante(int sante) {
		this.sante = sante;
	}
	/**
	 * Getter du seuil de santé critique 
	 * @return seuil de santé critique 
	 */
	public int getSeuilCritique() {
		return seuilCritique;
	}
	/**
	 * Setter du seuil de santé critique 
	 * @param seuilCritique
	 */
	public void setSeuilCritique(int seuilCritique) {
		this.seuilCritique = seuilCritique;
	}
	/**
	 * *********Getter*********
	 * @return List dépend de la classe d'énumération des terrains accessibles à chaque animal
	 */
	public List<Terrain> getTerrainPossible() {
		return terrainPossible;
	}
	/**
	 * *********Setter*********
	 * @param terrainPossible
	 */
	public void setTerrainPossible(List<Terrain> terrainPossible) {
		this.terrainPossible = terrainPossible;
	}
	/**
	 * *********Getter*********
	 * @return Case dépend de la classe Case implémentée dans le package Terrain retourne la position de l'animal sur la grille
	 */
	public Case getPosition() {
		return position;
	}
	/**
	 * *********Setter*********
	 * @param position
	 */
	public void setPosition(Case position) {
		this.position = position;
	}
	
	/**
	 * *********Getter*********
	 * @return int compteur du nombre de tour s'étant écoulé depuis la dernière reproduction
	 */
	public int getTpsRepos() {
		return tpsRepos;
	}
	/**
	 * *********Setter*********
	 * @param tpsRepos
	 */
	public void setTpsRepos(int tpsRepos) {
		this.tpsRepos = tpsRepos;
	}
	
	/**
	 * *********Getter*********
	 * @return int seuil minimal de temps à respecter pour pouvoir utiliser à nouveau la méthode seReproduire(Case)
	 */
	public int getTpsMinimumRepro() {
		return tpsMinimumRepro;
	}
	/**
	 * *********Setter*********
	 * @param tpsMinimumRepro
	 */
	public void setTpsMinimumRepro(int tpsMinimumRepro) {
		this.tpsMinimumRepro = tpsMinimumRepro;
	}
	/**
	 * *********Getter*********
	 * @return int age maximal que peut avoir l'animal
	 */
	public int getAgeMax() {
		return ageMax;
	}
	/**
	 * *********Setter*********
	 * @param int age max possible
	 */
	public void setAgeMax(int ageMax) {
		this.ageMax = ageMax;
	}
	
	/**
	 * Getter de l'attribut espèce
	 * @return espece
	 */
	public String getEspece() {
		return espece;
	}
	/**
	 * Setter de espèce
	 * @param espece
	 */
	public void setEspece(String espece) {
		this.espece = espece;
	}
	/**
	 * Getter du nombre maximal de petit par portée
	 * @return nombre maximal de petit par portée
	 */
	public int getNbMaxPetit() {
		return nbMaxPetit;
	}
	/**
	 * Getter du nombre maximal de petit par portée
	 * @param nbMaxPetit
	 */
	public void setNbMaxPetit(int nbMaxPetit) {
		this.nbMaxPetit = nbMaxPetit;
	}
	
	
	
	/*****************	Méthodes **************************
	
	
	
	/**
	 * Méthode pour incrémenter l'âge des animaux; la méthode doit vérifier une condition de vieillesse(si l'animal est trop vieux on change son boolean en false pour le faire mourir de vieillesse)
	 */
	public void vieillir() {
		this.setAge(this.getAge()+1);//est-ce bien neccessaire ?
		this.tpsRepos+=1; //on incrémente le temps depuis le dernier accouchement 
		if (this.age>this.ageMax ) {
			this.alive=false;
			this.setAge(0);//on remet l'age à zéro après la mort pour pouvoir compter le temps de décomposition
		}
		
	}
	
	
	/**
	 * Méthode prenant la grille en argument pour la scanner et renvoyer la position de l'animal de sexe opposé le plus proche; la méthode doit faire le test
	 * pour vérifier si le seuil avant reproduction est atteint  
	 * @return Case renvoie la position du partenaire potentiel le plus proche
	 */
	public Case reperageReproduction(Grille g) {
		int portee = this.getPm()+5;
		List<Case> ListeCases = new ArrayList<Case>();
		int repartition = (int) portee/2;
		int ligneDepart = this.getPosition().getLigne()-repartition;
		int colonneDepart =this.getPosition().getColonne()-repartition;
		int ligneArrivee = this.getPosition().getColonne()-repartition+portee-1;
		int colonneArrivee = this.getPosition().getColonne()-repartition+portee-1;
		for (int i=0; ligneDepart+i<ligneArrivee; i++) {
			for(int j=0; colonneDepart+j<colonneArrivee;j++) {
				ListeCases.add(g.getQuadrillage()[i][j]);
			}
		}
		List<Case> casesPartenaires = new ArrayList<Case>();
		List<Double> distPartenaires = new ArrayList<Double>();
		
		for (Case c : casesPartenaires) {
			List<Animal> contenu = c.getContenu();
			for (Animal a : contenu) {
				if (a.getEspece().equals(this.getEspece()) && !a.getSexe().equals(this.getSexe())){
					if ( a.getTpsRepos()> a.getTpsMinimumRepro()  && this.getTpsRepos()> this.getTpsMinimumRepro()) {
						casesPartenaires.add(c);
						
						double dist = Math.sqrt(Math.pow((this.getPosition().getLigne()-c.getLigne()),2)+Math.pow((this.getPosition().getColonne()-c.getColonne()),2));
						distPartenaires.add(dist);
					}
				}
			}
		}
		int index = distPartenaires.indexOf(Collections.min(distPartenaires));
		return casesPartenaires.get(index);
	}
	
	
	/**
	 * Méthode prenant en paramètre la grille pour renvoyer la position de ce que l'animal mange (permet d'avoir la direction dans laquelle l'animal doit se déplacer)
	 * @return Case renvoie la position du point de nourriture le plus proche (ou d'une proie)
	 */
	public abstract Case reperageNourriture(Grille g);
	
	/**
	 * Méthode permettant à l'animal de se nourrir prend en paramètre la faune(afin de pouvoir supprimer 
	 * les animaux mangés) augmente la santé de 1 
	 */
	public abstract void seNourrir(Faune f);
	
	/**
	 * Méthode permettant a deux animaux de sexes opposés de se reproduire 
	 * (permet de créer de nouvelles générations)
	 * 
	 * 
	 */
	public abstract void seReproduire(Faune f);
	
	
	/**
	 * Méthode permettant à chaque animal de se déplacer; doit dépendre du(des) milieu(x) au(x)quel(s) l'animal appartient
	 * @return renvoie une case (tuple de coordonnées x,y)
	 */
	public abstract void seDeplacer(Case c, Grille g);
	
	

	
	
}
