package terrain;

import java.util.List;
import Faune.Animal;

/**
 * Classe Case représentant une case individuelle de la grille, sa position, son contenu
 * @author Constance Barban, Quentin Le Port, Nathan Wolff
 *
 */

public class Case {
	
	/**
	 * Numéro de ligne dans la grille
	 */
	private int ligne;
	
	/**
	 * Numéro de colonne dans la grille
	 */
	private int colonne;
	
	/**
	 * Terrain correspondant à cette case
	 */
	private final Terrain terrainRéel;
	
	/**
	 * Liste possiblement vide des animaux positionnés sur cette case
	 */
	private List <Animal> contenu;
	
	/**
	 * Quantité de nourriture pour herbivore présente sur cette case
	 */
	private int nourriture;
	
	/**
	 * Constructeur d'une case
	 * @param l : ligne dans la grille
	 * @param c : colonne dans la grille
	 * @param t : terrain correspondant à la case
	 */
	public Case(int l, int c, Terrain t) {
		this.ligne = l;
		this.colonne = c;
		this.terrainRéel = Terrain.PLAINE;
		this.nourriture = 0;
		this.contenu = null;
	}

	/**
	 * Getter de la variable nourriture
	 * @return Int : quantité de nourriture pour herbivore présente sur la case
	 */
	public int getNourriture() {
		return nourriture;
	}

	/**
	 * Setter de la variable nourriture
	 * @param nourriture
	 */
	public void setNourriture(int nourriture) {
		this.nourriture = nourriture;
	}

	/**
	 * Getter de la variable ligne
	 * @return Int : ligne de la case dans la grille
	 */
	public int getLigne() {
		return ligne;
	}

	/**
	 * Getter de la variable colonne
	 * @return Int : colonne de la case dans la grille
	 */
	public int getColonne() {
		return colonne;
	}
	
	/**
	 * Getter de la variable terrainRéel
	 * @return Terrain : terrain de la case
	 */
	public Terrain getTerrainRéel() {
		return terrainRéel;
	}

	/**
	 * Getter de la variable contenu
	 * @return List <Animal> : liste des animaux présents sur la case
	 */
	public List<Animal> getContenu() {
		return contenu;
	}
	
	
	

}
