package terrain;


/**
 * Classe Grille représentant la zone de simulation sous forme de quadrillage
 * @author Constance Barban, Quentin Le Port, Nathan Wolff
 *
 */

public class Grille {
	
	/**
	 * Nombre de lignes composant la grille
	 */
	private int nbLigne;
	
	/**
	 * Nombre de colonne composant la grille
	 */
	private int nbColonne;
	
	/**
	 * Liste des cases composant la grille (liste de taille nbLigne * nbColonne)
	 */
	private Case[][] quadrillage;
	
	/**
	 * Constructeur d'une grille précisant son nombre de lignes et de colonne
	 * @param l
	 * @param c
	 */
	public Grille(int l, int c) {
		this.nbLigne = l;
		this.nbColonne = c;
		this.quadrillage = new Case[l][c];
		
		Terrain t;
		for (int i = 0; i < this.nbLigne; i ++) {
			for (int j = 0; j < this.nbColonne; j++) {
				if ((j % 4 == 0 || ((j + 1) % 4) == 0) && (i % 4 == 0 || ((i + 1 ) % 4) == 0)) {
					t = Terrain.FORÊT;
				}
				else if ((i == 5 || i == 6 || i == 25 || i == 26) || (j == 14 || j == 15)) {
					t = Terrain.EAU;
				}
				else {
					t = Terrain.PLAINE;
				}
				this.quadrillage[i][j] = new Case (i, j, t);
			}
		}
	}
	
	/**
	 * Constructeur d'un grille rectangulaire en ne connaissant qu'on facteur multiplicatif taille, valant 1, 2, 3...
	 * @param taille
	 */
	public Grille(int taille) {
		this.nbLigne = 20*taille;
		this.nbColonne = 40*taille;
		this.quadrillage = new Case[this.nbLigne][this.nbColonne];
		
		Terrain t;
		for (int i = 0; i < this.nbLigne; i ++) {
			for (int j = 0; j < this.nbColonne; j++) {
				if ((j % 4 == 0 || ((j + 1) % 4) == 0) && (i % 4 == 0 || ((i + 1 ) % 4) == 0)) {
					t = Terrain.FORÊT;
				}
				else if (((i == 25 || i == 26) && j < 40) || (j == 14 || j == 15)) {
					t = Terrain.EAU;
				}
				else {
					t = Terrain.PLAINE;
				}
				this.quadrillage[i][j] = new Case (i, j, t);
			}
		}
	}

	/**
	 * Getter de la variable nbLigne
	 * @return Int : le nombre de lignes
	 */
	public int getNbLigne() {
		return nbLigne;
	}

	/**
	 * Setter du nombre de lignes
	 * @param nbLigne
	 */
	public void setNbLigne(int nbLigne) {
		this.nbLigne = nbLigne;
	}

	/**
	 * Getter de la variable nbColonne
	 * @return Int : le nombre de colonnes
	 */
	public int getNbColonne() {
		return nbColonne;
	}

	/**
	 * Setter du nombre de colonnes
	 * @param nbColonne
	 */
	public void setNbColonne(int nbColonne) {
		this.nbColonne = nbColonne;
	}

	/**
	 * Getter de la variable quadrillage
	 * @return List <Case> : la liste des cases
	 */
	public Case[][] getQuadrillage() {
		return quadrillage;
	}
	

}
