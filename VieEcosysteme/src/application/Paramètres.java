package application;

/**
 * Classe Paramètres : classe de constantes définissant les caractéristiques de la simulation
 * telles que le nombre de tours par exemple.
 * Certains de ces paramètres peuvent être modifiés par l'utilisateur.
 * 
 * @author Constance Barban, Quentin Le Port, Nathan Wolff
 *
 */

public class Paramètres {
	
	/**
	 * Nombre de tours au bout duquel se termine la simulation
	 */
	@SuppressWarnings("unused")
	private final int nbTours;
	
	/**
	 * Nombre de tours espaçant deux éruptions du volcan
	 */
	@SuppressWarnings("unused")
	private final int tpsEntreEruption;
	
	/**
	 * Distance (en nombre de cases) jusqu'à laquelle l'éruption fait effet
	 * à partir d'une case de type Volcan
	 */
	@SuppressWarnings("unused")
	private final int portéeEruption;

	/**
	 * Constructeur par défaut, dans le cas où l'utilisateur ne modifie pas le nombre de tours
	 */
	public Paramètres() {
		this.nbTours = 100;
		this.tpsEntreEruption = 50;
		this.portéeEruption = 10;
	}
	
	/**
	 * Constructeur prenant en compte une demande de l'utilisateur sur le nombre de tours
	 * @param n : le nombre de tours spécifié par l'utilisateur
	 */
	public Paramètres(int n) {
		this.nbTours = n;
		this.tpsEntreEruption = 50;
		this.portéeEruption = 10;
	}
	
	/**
	 * Getter de la variable nbTours
	 * @return valeur de nbTours
	 */
	public int getNbTours() {
		return this.nbTours;
	}

	/**
	 * Getter de la variable tpsEntreEruption
	 * @return valeur de tpsEntreEruption
	 */
	public int getTpsEntreEruption() {
		return tpsEntreEruption;
	}

	/**
	 * Getter de la variable portéeEruption
	 * @return valeur de portéeEruption
	 */
	public int getPortéeEruption() {
		return portéeEruption;
	}
	
	
}
