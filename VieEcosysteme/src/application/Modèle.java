package application;

import Faune.Faune;
import terrain.Grille;

/**
 * Classe Modèle représentant l'application en elle-même, qui lance la simulation.
 * Chaque partie est une instance de cette classe.
 * @author Constance Barban, Quentin Le Port, Nathan Wolff
 *
 */

public class Modèle {
	
	/**
	 * Quadrillage sur leuquel évoluent les animaux lors de la simulation
	 */
	private Grille grille;
	
	/**
	 * Liste des animaux composant l'écosystème
	 */
	private Faune écosystème;
	
	/**
	 * Ensemble des constantes définies définitivement pour la simulation
	 */
	private Paramètres règles;
	
	/**
	 * Constructeur de modèle par défaut
	 */
	public Modèle() {
		this.écosystème = new Faune();
		this.grille = new Grille();
		this.règles = new Paramètres();
	}
	
	/**
	 * Méthode lançant une partie et la laissant se dérouler
	 */
	public void lancerSimulation() {
		
		int tour = 0;
		while (tour < this.getRègles().getNbTours()) {
			tour = tour + 1;
		}
		System.out.println("yo on a fait " + tour + " tours !");
	}
	
	/**
	 * Méthode permettant à l'utilisateur de modifier les paramètres de la simulation
	 * comme le nombre de tours, la taille de la grille etc...
	 */
	public void définirParamètres() {
		
	}

	/**
	 * Getter de la variable grille
	 * @return Grille : grille permettant la simulation
	 */
	public Grille getGrille() {
		return grille;
	}

	/**
	 * Setter de la variable grille
	 * @param grille
	 */
	public void setGrille(Grille grille) {
		this.grille = grille;
	}

	/**
	 * Getter de la variable écosystème
	 * @return Faune : la liste des animaux
	 */
	public Faune getÉcosystème() {
		return écosystème;
	}

	/**
	 * Setter de la variable écosystème
	 * @param grille
	 */
	public void setÉcosystème(Faune écosystème) {
		this.écosystème = écosystème;
	}

	/**
	 * Getter de la variable règles
	 * @return Paramètres : constantes de la simulation
	 */
	public Paramètres getRègles() {
		return règles;
	}

	/**
	 * Setter de la variable règles
	 * @param règles
	 */
	public void setRègles(Paramètres règles) {
		this.règles = règles;
	}

}
