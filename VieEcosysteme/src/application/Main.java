package application;

import affichageGraphique.testJava2D;
import terrain.Case;
import terrain.Terrain;

/**
 * Main du projet permettant de lancer autant de simulations que l'on souhaite
 * @author Constance Barban, Quentin Le Port, Nathan Wolff
 *
 */

public class Main {

	public static void main(String[] args) {
		
		//lancement de la fenêtre
		testJava2D app = new testJava2D();
		
		//lancement de l'aplication en elle-même
		Modèle test = new Modèle();
		test.lancerSimulation();
	
		System.out.println((int)5.0/2);
		
		Case p = new Case(2,2,Terrain.EAU);
		p.rotate(180);
	}

}
